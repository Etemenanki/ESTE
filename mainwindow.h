#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_FORWARD_DECLARE_CLASS(QTextEdit)
QT_FORWARD_DECLARE_CLASS(QMenu)
QT_FORWARD_DECLARE_CLASS(QToolBar)
QT_FORWARD_DECLARE_CLASS(QStatusBar)
QT_FORWARD_DECLARE_CLASS(QAction)

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);

private slots:
    void newFile();
    void openFile();
    void saveFile();
    void preferencesDialog();

    void writeSettings();

private:
    void setupActions();
    void setupMenuBar();
    void setupToolBar();
    void setupStatusBar();
    
    void readSettings();    
    
    QTextEdit *textEdit;
    
    QAction *menuToolBarAct;
    QAction *fileNewAct;
    QAction *fileOpenAct;
    QAction *fileSaveAct;
    QAction *fileExitAct;

    QMenu *fileMenuBar;
    QMenu *editMenuBar;
    QMenu *toolsMenuBar;
    QMenu *helpMenuBar;

    QToolBar *menuToolBar;
    QToolBar *fileToolBar;
    
    QStatusBar *statusBar;
};

#endif


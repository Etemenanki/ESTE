/*******************************************************************************
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
*******************************************************************************/


#include <QAction>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QFont>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPoint>
#include <QSettings>
#include <QSize>
#include <QStatusBar>
#include <QTextEdit>
#include <QTextStream>
#include <QToolBar>
#include <QVariant>
#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    // Main Window Features.
    this->textEdit = new QTextEdit;
    this->setCentralWidget(textEdit);
    this->setMinimumSize(400, 300);

    this->setFont(QFont("DejaVuSans", 9));

    setupActions();
    setupMenuBar();
    setupToolBar();
    setupStatusBar();

    readSettings();
    writeSettings();

    
}

    
void MainWindow::setupActions()
{
    // Main menu actions.
    menuToolBarAct = new QAction(QIcon("./icons/configure.png"), tr("&Menu"), this);
    connect(menuToolBarAct, SIGNAL(triggered()), this, SLOT(preferencesDialog()));

    // File menu actions.
    fileNewAct = new QAction(QIcon("./icons/file-new.png"), tr("&New..."), this);
    fileNewAct->setShortcuts(QKeySequence::New);
    fileNewAct->setToolTip(tr("New..."));
    fileNewAct->setWhatsThis(tr("New..."));
    fileNewAct->setStatusTip(tr("New..."));
    connect(fileNewAct, SIGNAL(triggered()), this, SLOT(newFile()));

    fileOpenAct = new QAction(QIcon("./icons/file-open.png"), tr("&Open..."), this);
    fileOpenAct->setShortcuts(QKeySequence::Open);
    fileOpenAct->setToolTip(tr("Open file"));
    fileOpenAct->setWhatsThis(tr("Open file"));
    fileOpenAct->setStatusTip(tr("Open file"));
    connect(fileOpenAct, SIGNAL(triggered()), this, SLOT(openFile()));

    fileSaveAct = new QAction(QIcon("./icons/file-save.png"), tr("&Save"), this);
    fileSaveAct->setShortcuts(QKeySequence::Save);
    fileSaveAct->setToolTip(tr("Save"));
    fileSaveAct->setWhatsThis(tr("Save"));
    fileSaveAct->setStatusTip(tr("Save"));
    connect(fileSaveAct, SIGNAL(triggered()), this, SLOT(saveFile()));

    fileExitAct = new QAction(QIcon("./icons/file-exit.png"), tr("&Exit..."), this);
    connect(fileExitAct, SIGNAL(triggered()), this, SLOT(writeSettings()));
    connect(fileExitAct, SIGNAL(triggered()), this, SLOT(close()));

    //connect(salir, SIGNAL(), qApp, SLOT());
}

    
void MainWindow::setupMenuBar()
{
    // File menubar.
    fileMenuBar = menuBar()->addMenu(tr("&File"));
    fileMenuBar->addAction(fileNewAct);
    fileMenuBar->addAction(fileOpenAct);
    fileMenuBar->addAction(fileSaveAct);
    fileMenuBar->addAction(fileExitAct);

    // Edit menubar.
    editMenuBar = menuBar()->addMenu(tr("&Edit"));
    toolsMenuBar = menuBar()->addMenu(tr("&Tools"));
    helpMenuBar = menuBar()->addMenu(tr("&Help"));
}
    

void MainWindow::setupToolBar()
{
    // Main menu toolbar.
    menuToolBar = new QToolBar(tr("&Menu"), this);
    addToolBar(Qt::RightToolBarArea, menuToolBar); 
    menuToolBar->setOrientation(Qt::Vertical);
    menuToolBar->setAllowedAreas(Qt::AllToolBarAreas);
    menuToolBar->setMovable(false);
    menuToolBar->addAction(menuToolBarAct);

    // File toolbar.
    fileToolBar = new QToolBar(tr("&File"), this);
    addToolBar(Qt::RightToolBarArea, fileToolBar);
    fileToolBar->setOrientation(Qt::Vertical);
    fileToolBar->setAllowedAreas(Qt::AllToolBarAreas);
    fileToolBar->setMovable(false);
    fileToolBar->addAction(fileNewAct);
    fileToolBar->addAction(fileOpenAct);
    fileToolBar->addAction(fileSaveAct);
    fileToolBar->addAction(fileExitAct);
}


void MainWindow::setupStatusBar()
{
    // Status Bar.
    statusBar = new QStatusBar;
    setStatusBar(statusBar);
}


void MainWindow::preferencesDialog()
{
    qDebug("New!");
}


/* newFile Private Slot.
 * Borra el texto anterior.
 *
 */
void MainWindow::newFile()
{
    if (!textEdit->document()->isEmpty()) {
        QMessageBox msgBoxIsModified;
        msgBoxIsModified.setText(tr("The current document has been modified."));
        msgBoxIsModified.setInformativeText(tr("Do you want to save your"
                                               "changes?"));
        msgBoxIsModified.setStandardButtons(QMessageBox::Save |
                                        QMessageBox::Discard | QMessageBox::Cancel);
        msgBoxIsModified.setDefaultButton(QMessageBox::Save);
        msgBoxIsModified.setIcon(QMessageBox::Information);

        int rec = msgBoxIsModified.exec();

        switch (rec) {
            case QMessageBox::Save:
                saveFile();
            case QMessageBox::Discard:
                textEdit->document()->clear();
            case QMessageBox::Cancel:
                break;
            default:
                break;
        }
    }
    else {
        return; 
    }
}


/* openFile Private Slot.
 * 
 *   QString fileName, abre el diálogo de abrir archivo y almacena el nombre del
 *          archivo elegido.
 *   QFile file(fileName), 
 *   QTextStream in(&file)
 */
void MainWindow::openFile()
{
    if (textEdit->document()->isEmpty()) {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Load file"));    
        if (fileName.isEmpty()) {
            return;
        }
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly)) {
            QString msg = tr("Failed to open %1\n%2").arg(fileName)
                                                     .arg(file.errorString());
            QMessageBox::warning(this, tr("Error"), msg);
            return;
        }
        QTextStream in(&file);
        textEdit->setPlainText(in.readAll());
        statusBar->showMessage(tr("File loaded"), 2000);
    }
    else {
        QString msg2 = tr("Current document has been modified.");
        QMessageBox::information(this, tr("Information"), msg2);
        return;
    }
}


void MainWindow::saveFile()
{
    if (!textEdit->document()->isEmpty()) {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save file"));
        if (fileName.isEmpty()) {
            return;
        }
        QFile file(fileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            QString msg = tr("Failed to write %1\n%2").arg(fileName)
                                                      .arg(file.errorString());
            QMessageBox::warning(this, tr("Error"), msg);
            return;
        }
        QTextStream out(&file);
        out << textEdit->toPlainText();
        statusBar->showMessage(tr("File saved"), 2000);
        return;
    }
    else {
        return;
    }
}


void MainWindow::writeSettings()
{
    QSettings settings("./simpledit.conf", QSettings::NativeFormat);
    settings.beginGroup("window");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    //settings.setValue("font", font());
    settings.endGroup();
}


void MainWindow::readSettings()
{
    QSettings settings("./simpledit.conf", QSettings::NativeFormat);
    settings.beginGroup("window");
    resize(settings.value("size").toSize());
    move(settings.value("pos").toPoint());
    //setFont(QFont(settings.value("font").toString()));
    settings.endGroup();
}


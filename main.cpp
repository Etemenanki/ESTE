/*

    Probando Qt5

*/


#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    //Q_INIT_RESOURCE(simpledit);

    QApplication SimplEditApp(argc, argv);
    SimplEditApp.setOrganizationName("PocoSoft");
    SimplEditApp.setOrganizationDomain("pocosoft.com");
    SimplEditApp.setApplicationName("Este");
    SimplEditApp.setApplicationVersion("0.5");
    
    MainWindow mainWin;
    mainWin.setObjectName("MainWindow");
    mainWin.setWindowTitle("Este");
    mainWin.setWindowIcon(QIcon("./icons/simplEdit.png"));
    mainWin.show();

    return SimplEditApp.exec();
}

